package com.hw.db.controllers;

import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.Post;
import com.hw.db.models.Thread;
import com.hw.db.models.User;
import com.hw.db.models.Vote;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class ThreadControllerTests {
    private ThreadController controller;
    private final String username = "username test";
    private User user;
    private Post post;
    private Thread thread;
    private final int threadId = 1234;
    private final String slug = "slug example";
    private List<Post> postList;

    @BeforeEach
    void setup() {
        Timestamp now = Timestamp.from(Instant.now());
        final String forum = "test forum";
        user = new User(username, "test@test.test", "Test", "test test test");
        post = new Post(username, now, forum, "test test", 0, 0, false);
        postList = new ArrayList<>();
        postList.add(post);
        controller = new ThreadController();
        thread = new Thread(username, now, forum, "test thread message", slug, "Test Thread Title", 3);
        thread.setId(threadId);
    }

    @Test
    @DisplayName("Get thread by slug and ID")
    void checkThreadBySlug() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thread);
            assertEquals(controller.CheckIdOrSlug(slug), thread);
            assertNull(controller.CheckIdOrSlug("missing"));
        }
    }

    @Test
    @DisplayName("Get thread by ID")
    void checkThreadById() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadById(threadId)).thenReturn(thread);
            assertEquals(controller.CheckIdOrSlug(Integer.toString(threadId)), thread);
            assertNull(controller.CheckIdOrSlug("1111"));
        }
    }

    @Test
    @DisplayName("Create a post")
    void createPost() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
                threadMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thread);
                userMock.when(() -> UserDAO.Info(username)).thenReturn(user);
                assertEquals(ResponseEntity.status(HttpStatus.CREATED).body(postList),
                        controller.createPost(slug, postList));
                assertEquals(thread, ThreadDAO.getThreadBySlug(slug));
            }
        }
    }

    @Test
    @DisplayName("Get posts")
    void PostsTest() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thread);
            threadMock.when(() -> ThreadDAO.getPosts(threadId, 1, 100, "flat", false))
                    .thenReturn(postList);
            threadMock.when(() -> ThreadDAO.getPosts(threadId, 0, 100, "flat", false))
                    .thenReturn(Collections.emptyList());
            assertEquals(ResponseEntity.status(HttpStatus.OK).body(postList),
                    controller.Posts(slug, 1, 100, "flat", false));
            assertEquals(ResponseEntity.status(HttpStatus.OK).body(Collections.emptyList()),
                    controller.Posts(slug, 0, 100, "flat", false));
        }
    }

    @Test
    @DisplayName("Change thread")
    void changeThread() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            final String slug2 = "another slug";
            final Timestamp ts2 = Timestamp.valueOf("2023-11-11 11:11:11");
            final int threadId2 = 946156;
            final Thread thread2 = new Thread(username, ts2, "test forum", "test msg", slug2, "test title", 4);
            thread2.setId(threadId2);
            threadMock.when(() -> ThreadDAO.getThreadBySlug(slug2)).thenReturn(thread2);
            threadMock.when(() -> ThreadDAO.getThreadById(threadId2)).thenReturn(thread2);

            threadMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thread);

            threadMock.when(() -> ThreadDAO.change(thread, thread2)).thenAnswer((invocationOnMock) -> {
                threadMock.when(() -> ThreadDAO.getThreadBySlug(slug2)).thenReturn(thread2);
                return null;
            });

            assertEquals(ResponseEntity.status(HttpStatus.OK).body(thread2),
                    controller.change(slug2, thread2));
            assertEquals(thread2, controller.CheckIdOrSlug(slug2));
        }
    }

    @Test
    @DisplayName("Get thread details")
    void threadInfo() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thread);
            assertEquals(ResponseEntity.status(HttpStatus.OK).body(thread), controller.info(slug));
            threadMock.when(() -> ThreadDAO.getThreadBySlug("missing")).thenThrow(new DataAccessException("") {
            });
            assertEquals(ResponseEntity.status(HttpStatus.NOT_FOUND).body(null).getStatusCode(),
                    controller.info("missing").getStatusCode());
        }
    }

    @Test
    @DisplayName("Create Vote")
    void createVote() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
                userMock.when(() -> UserDAO.Info(username)).thenReturn(user);
                final Vote vote = new Vote(username, 1);
                threadMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thread);
                threadMock.when(() -> ThreadDAO.change(vote, thread.getVotes())).thenReturn(thread.getVotes());
                assertEquals(ResponseEntity.status(HttpStatus.OK).body(thread),
                        controller.createVote(slug, vote));
            }
        }
    }
}
